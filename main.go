package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

var T = "what is this \"kerning\" that you speak of?"

// A quoting system consists of an opening and closing pair,
// and a pair to be used within a quote.
type Q struct {
	name                string
	opening, closing    string
	recursin, recursout string // quotes inside quotes
}

var Qs = []Q{
	{"North American", "“", "”", "‘", "’"},
	{"British", "‘", "’", "“", "”"}, // Bringhurst
	{"French", "« ", " »", "‹ ", " ›"},
	{"Danish", "» ", " «", "› ", " ‹"}, // Wikipedia
	{"German", "„", "“", "‚", "‘"},     // Wikipedia
	{"Hungarian", "„", "”", "»", "«"},  // Wikipedia
	{"Spanish", "«", "»", "“", "”"},    // Wikipedia
	{"Swedish", "”", "”", "’", "’"},    // Wikipedia
}

func main() {
	w, err := os.OpenFile("index.md",
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	for _, q := range Qs {
		o := strings.Replace(T, "\"", q.recursin, 1)
		b := strings.Replace(o, "\"", q.recursout, 1)
		fmt.Fprintf(w, "- %s  \nThey asked %s%s%s\n",
			q.name,
			q.opening, b, q.closing)
	}

	fmt.Fprintf(w, `---
This is just a sampling of the major forms.
Most of these are used in more than one country or
region and have variations beyond those given here.

From a typographical and type designer’s perspective,
it is pretty clear that one should expect any style of quote
to be used in any position, top-line or base-line, opening or closing,
forward or reversed, with or without space,
in obverse and reflected forms,
with quotes facing each other or with quotes facing the same way.
`)
}
